#include <stdio.h>            /* C input/output                       */
#include <stdlib.h>           /* C standard library                   */
#include <glpk.h>             /* GNU GLPK linear/mixed integer solver */
#include <time.h>
#include <math.h>

#define M_PI 3.14159265358979323846

int main(void)
{
  /* declare variables */
  glp_prob *lp;
  int ia[1+1000], ja[1+1000];
  double ar[1+1000], z, s1, s2, s3, f1, f2, f3, f4, f5 ,f6;
  /* create problem */
  lp = glp_create_prob();
  glp_set_prob_name(lp, "motor");
  glp_set_obj_dir(lp, GLP_MIN);
  /* fill problem */
  glp_add_rows(lp, 9);

  glp_smcp params;
  glp_init_smcp(&params);
  printf("%d \n",params.msg_lev);
  params.msg_lev = 1;
  printf("%d \n",params.msg_lev);

  double F[3] = {1,-0.5,0};
  double M[3] = {0.1,-0.2,0.3};

  double c = 0.016;
  double theta = 0.35;
  double l = 0.3;
  double f_max = 10;

  double BF[3][6], BM[3][6];
  int i;

  for (i=1;i<7;i++){
    double in = (float) i;
    BF[0][i-1] = pow(-1,in)*sin(theta)*sin(M_PI*(in-1.0)/3.0);
    BF[1][i-1] = pow(-1,in-1)*sin(theta)*cos(M_PI*(in-1.0)/3.0);
    BF[2][i-1] = cos(theta);

    BM[0][i-1] = -sin(M_PI*(in-1.0)/3.0)*(c*sin(theta)-l*cos(theta));
    BM[1][i-1] = cos(M_PI*(in-1.0)/3.0)*(c*sin(theta)-l*cos(theta));
    BM[2][i-1] = pow(-1,in-1)*(c*cos(theta)+l*sin(theta));  
  }

  int N = 10000;
  clock_t start, start_in, end, end_in;
  double cpu_avg_time_used, time_used[N];

  glp_set_row_name(lp, 1, "p1");
  glp_set_row_name(lp, 2, "p2");
  glp_set_row_name(lp, 3, "q1");
  glp_set_row_name(lp, 4, "q2");
  glp_set_row_name(lp, 5, "r1");
  glp_set_row_name(lp, 6, "r2");
  glp_add_cols(lp, 9);

  glp_set_col_name(lp, 1, "s1");
  glp_set_col_bnds(lp, 1, GLP_LO, 0.0, 1.0);
  glp_set_obj_coef(lp, 1, 1);
  glp_set_col_name(lp, 2, "s2");
  glp_set_col_bnds(lp, 2, GLP_LO, 0.0, 1.0);
  glp_set_obj_coef(lp, 2, 1);
  glp_set_col_name(lp, 3, "s3");
  glp_set_col_bnds(lp, 3, GLP_LO, 0.0, 1.0);
  glp_set_obj_coef(lp, 3, 1);

  glp_set_col_name(lp, 4, "f1");
  glp_set_col_bnds(lp, 4, GLP_DB, 0.0, f_max);
  glp_set_obj_coef(lp, 4, 0);
  glp_set_col_name(lp, 5, "f2");
  glp_set_col_bnds(lp, 5, GLP_DB, 0.0, f_max);
  glp_set_obj_coef(lp, 5, 0);
  glp_set_col_name(lp, 6, "f3");
  glp_set_col_bnds(lp, 6, GLP_DB, 0.0, f_max);
  glp_set_obj_coef(lp, 6, 0);
  glp_set_col_name(lp, 7, "f4");
  glp_set_col_bnds(lp, 7, GLP_DB, 0.0, f_max);
  glp_set_obj_coef(lp, 7, 0);
  glp_set_col_name(lp, 8, "f5");
  glp_set_col_bnds(lp, 8, GLP_DB, 0.0, f_max);
  glp_set_obj_coef(lp, 8, 0);
  glp_set_col_name(lp, 9, "f6");
  glp_set_col_bnds(lp, 9, GLP_DB, 0.0, f_max);
  glp_set_obj_coef(lp, 9, 0);

  int count = 1, row = 1, col = 1;
  /* First force row -s_1 - BF_1 f <= -F_1 */
  ia[count] = row, ja[count] = col, ar[count] = -1.0; /* a[1,1] = -1 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,3] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[0][0];  /* a[1,4] = -BF(0,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[0][1];  /* a[1,5] = -BF(0,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[0][2];  /* a[1,6] = -BF(0,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[0][3];  /* a[1,7] = -BF(0,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[0][4];  /* a[1,8] = -BF(0,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[0][5];  /* a[1,9] = -BF(0,5) */
  count++, col=1 ,row++;

  /* Second force row -s_1 + BF_1 f <= F_1 */
  ia[count] = row, ja[count] = col, ar[count] = -1.0; /* a[1,1] = -1 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,3] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[0][0];  /* a[1,4] = -BF(0,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[0][1];  /* a[1,5] = -BF(0,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[0][2];  /* a[1,6] = -BF(0,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[0][3];  /* a[1,7] = -BF(0,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[0][4];  /* a[1,8] = -BF(0,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[0][5];  /* a[1,9] = -BF(0,5) */
  count++, col=1 ,row++;

  /* Third force row -s_2 - BF_2 f <= -F_2 */
  ia[count] = row, ja[count] = col, ar[count] = 0.0; /* a[1,1] = -1 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -1.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,3] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[1][0];  /* a[1,4] = -BF(1,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[1][1];  /* a[1,5] = -BF(1,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[1][2];  /* a[1,6] = -BF(1,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[1][3];  /* a[1,7] = -BF(1,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[1][4];  /* a[1,8] = -BF(1,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[1][5];  /* a[1,9] = -BF(1,5) */
  count++, col=1 ,row++;

  /* Fourth force row -s_2 + BF_2 f <= F_2 */
  ia[count] = row, ja[count] = col, ar[count] = 0.0; /* a[1,1] = -1 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -1.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,3] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[1][0];  /* a[1,4] = -BF(1,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[1][1];  /* a[1,5] = -BF(1,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[1][2];  /* a[1,6] = -BF(1,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[1][3];  /* a[1,7] = -BF(1,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[1][4];  /* a[1,8] = -BF(1,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[1][5];  /* a[1,9] = -BF(1,5) */
  count++, col=1 ,row++;

  /* Fifth force row -s_3 - BF_3 f <= -F_3 */
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,1] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -1.0; /* a[1,3] = -1 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[2][0];  /* a[1,4] = -BF(1,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[2][1];  /* a[1,5] = -BF(1,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[2][2];  /* a[1,6] = -BF(1,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[2][3];  /* a[1,7] = -BF(1,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[2][4];  /* a[1,8] = -BF(1,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -BF[2][5];  /* a[1,9] = -BF(1,5) */
  count++, col=1 ,row++;

  /* Sixth force row -s_3 + BF_3 f <= F_3 */
  ia[count] = row, ja[count] = col, ar[count] = 0.0; /* a[1,1] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = -1.0;  /* a[1,3] = -1 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[2][0];  /* a[1,4] = BF(2,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[2][1];  /* a[1,5] = BF(2,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[2][2];  /* a[1,6] = BF(2,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[2][3];  /* a[1,7] = BF(2,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[2][4];  /* a[1,8] = BF(2,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BF[2][5];  /* a[1,9] = BF(2,5) */
  count++, col=1 ,row++;

  /* First moment row BM_1 f = M_1*/
  ia[count] = row, ja[count] = col, ar[count] = 0.0; /* a[1,1] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,3] = -1 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[0][0];  /* a[1,4] = BM(0,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[0][1];  /* a[1,5] = BM(0,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[0][2];  /* a[1,6] = BM(0,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[0][3];  /* a[1,7] = BM(0,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[0][4];  /* a[1,8] = BM(0,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[0][5];  /* a[1,9] = BM(0,5) */
  count++, col=1 ,row++;

  /* Second moment row BM_2 f = M_2*/
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,1] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,3] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[1][0];  /* a[1,4] = BM(1,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[1][1];  /* a[1,5] = BM(1,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[1][2];  /* a[1,6] = BM(1,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[1][3];  /* a[1,7] = BM(1,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[1][4];  /* a[1,8] = BM(1,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[1][5];  /* a[1,9] = BM(1,5) */
  count++, col=1 ,row++;

  /* Third moment row BM_3 f = M_3*/
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,1] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,2] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = 0.0;  /* a[1,3] = 0 */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[2][0];  /* a[1,4] = BM(2,0) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[2][1];  /* a[1,5] = BM(2,1) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[2][2];  /* a[1,6] = BM(2,2) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[2][3];  /* a[1,7] = BM(2,3) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[2][4];  /* a[1,8] = BM(2,4) */
  count++, col++;
  ia[count] = row, ja[count] = col, ar[count] = BM[2][5];  /* a[1,9] = BM(2,5) */

  start = clock();
  
  for (i=0;i<N;i++){

    start_in = clock();

    glp_set_row_bnds(lp, 1, GLP_UP, 0.0, -F[0]);
    glp_set_row_bnds(lp, 2, GLP_UP, 0.0,  F[0]);  
    glp_set_row_bnds(lp, 3, GLP_UP, 0.0, -F[1]);
    glp_set_row_bnds(lp, 4, GLP_UP, 0.0,  F[1]);  
    glp_set_row_bnds(lp, 5, GLP_UP, 0.0, -F[2]);
    glp_set_row_bnds(lp, 6, GLP_UP, 0.0,  F[2]);    
    glp_set_row_bnds(lp, 7, GLP_FX, M[0],  M[0]);  
    glp_set_row_bnds(lp, 8, GLP_FX, M[1],  M[1]);
    glp_set_row_bnds(lp, 9, GLP_FX, M[2],  M[2]);   

    glp_load_matrix(lp, count, ia, ja, ar);
    /* solve problem */
    glp_smcp params;
    glp_init_smcp(&params);
    params.msg_lev = 1;
    glp_simplex(lp, &params);
    /* recover and display results */
    z = glp_get_obj_val(lp);
    s1 = glp_get_col_prim(lp, 1);
    s2 = glp_get_col_prim(lp, 2);
    s3 = glp_get_col_prim(lp, 3);
    f1 = glp_get_col_prim(lp, 4);
    f2 = glp_get_col_prim(lp, 5);
    f3 = glp_get_col_prim(lp, 6);
    f4 = glp_get_col_prim(lp, 7);
    f5 = glp_get_col_prim(lp, 8);
    f6 = glp_get_col_prim(lp, 9);    

    end_in = clock();

    time_used[i] = ((double) (end_in - start_in)) / CLOCKS_PER_SEC;
  }

  end = clock();
     
  cpu_avg_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

  printf("f1 = %f; f2 = %f; f3 = %f; f4 = %f; f5 = %f; f6 = %f\n",f1,f2,f3,f4,f5,f6);
  printf("s1 = %f; s2 = %f; s3 = %f\n",s1,s2,s3);

  double Ma[3];

  Ma[0] = BM[0][0]*f1 + BM[0][1]*f2 + BM[0][2]*f3 + BM[0][3]*f4 + BM[0][4]*f5 + BM[0][5]*f6;
  Ma[1] = BM[1][0]*f1 + BM[1][1]*f2 + BM[1][2]*f3 + BM[1][3]*f4 + BM[1][4]*f5 + BM[1][5]*f6;
  Ma[2] = BM[2][0]*f1 + BM[2][1]*f2 + BM[2][2]*f3 + BM[2][3]*f4 + BM[2][4]*f5 + BM[2][5]*f6;

  printf("Ma1 = %f; Ma2 = %f; Ma3 = %f\n",Ma[0],Ma[1],Ma[2]);
  printf("M1 = %f; M2 = %f; M3 = %f\n",M[0],M[1],M[2]);


  cpu_avg_time_used = cpu_avg_time_used/N;
  printf("avg time (ms): %f \n",1000*cpu_avg_time_used);

  double sig = 0, avg = 0;
  for (i=0;i<N;i++){
    sig += pow(time_used[i]-cpu_avg_time_used,2);
    avg += time_used[i];
  }

  sig = sqrt(1.0/(N-1.0)*sig);
  avg = avg/N;
  printf("std time(ms): %f \n", 1000*sig);

  /* housekeeping */
  glp_delete_prob(lp);
  glp_free_env();
  return 0;
}