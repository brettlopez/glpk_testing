#include <stdio.h>            /* C input/output                       */
#include <stdlib.h>           /* C standard library                   */
#include <glpk.h>             /* GNU GLPK linear/mixed integer solver */
#include <time.h>

int main(void)
{
  /* declare variables */
  glp_prob *lp;
  int ia[1+1000], ja[1+1000];
  double ar[1+1000], z, x1, x2, x3;
  /* create problem */
  lp = glp_create_prob();
  glp_set_prob_name(lp, "short");
  glp_set_obj_dir(lp, GLP_MAX);
  /* fill problem */
  glp_add_rows(lp, 3);



  glp_smcp params;
  glp_init_smcp(&params);
  printf("%d \n",params.msg_lev);
  params.msg_lev = 1;
  printf("%d \n",params.msg_lev);


  int N = 1000,i;
  clock_t start, end;
  double cpu_avg_time_used;

  glp_set_row_name(lp, 1, "p");
  glp_set_row_name(lp, 2, "q");
  glp_add_cols(lp, 2);
  glp_set_col_name(lp, 1, "x1");
  glp_set_col_bnds(lp, 1, GLP_DB, 0.0, 1.0);
  glp_set_obj_coef(lp, 1, 0.6);
  glp_set_col_name(lp, 2, "x2");
  glp_set_col_bnds(lp, 2, GLP_DB, 0.0, 1.0);
  glp_set_obj_coef(lp, 2, 0.5);

  for (i=0;i<N;i++){
    start = clock();
    glp_set_row_bnds(lp, 1, GLP_UP, 0.0, 1.0);
    glp_set_row_bnds(lp, 2, GLP_UP, 0.0, 2.0);    
    
    ia[1] = 1, ja[1] = 1, ar[1] = 1.0; /* a[1,1] = 1 */
    ia[2] = 1, ja[2] = 2, ar[2] = 2.0; /* a[1,2] = 2 */
    ia[3] = 2, ja[3] = 1, ar[3] = 3.0; /* a[2,1] = 3 */
    ia[4] = 2, ja[4] = 2, ar[4] = 1.0; /* a[2,2] = 1 */
    glp_load_matrix(lp, 4, ia, ja, ar);
    /* solve problem */
    // glp_interior(lp, &params);
    glp_simplex(lp, &params);
    /* recover and display results */
    z = glp_get_obj_val(lp);
    x1 = glp_get_col_prim(lp, 1);
    x2 = glp_get_col_prim(lp, 2);
    end = clock();
     
   cpu_avg_time_used += ((double) (end - start)) / CLOCKS_PER_SEC;
  }

  printf("z = %f; x1 = %f; x2 = %f\n", z, x1, x2);

  cpu_avg_time_used = cpu_avg_time_used/N;
  printf("avg time (ms): %f \n",1000*cpu_avg_time_used);

  /* housekeeping */
  glp_delete_prob(lp);
  glp_free_env();
  return 0;
}